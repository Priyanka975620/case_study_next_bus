README 

NEXT_BUS

	next_bus.pl is a perl script which is used to get information of the next bus that is running
	along the given route, bus stop and a specific direction.Here, bus route, Bus stop name and
	the direction are the mandatory parameters that are expected from the user. 

AUTHOR

	Priyanka M

PREREQUISITES 

	To run the script, it is mandatory to have perl installed on the machine running it and also the
	modules used in the script

	Modules Used:

	** LWP::Simple; 

PARAMETERS

	Bus_Route 	  - This should have the bus route or atleast a unique sub string of it that
					the user want to travel in from a particular bus stop

	Bus_Stop_Name - This should have the bus stop name that the user want to board or check
					at what time the bus comes to that particular bus stop.
					Also, this should have atleast a unique sub string of it.

	Direction 	  - This should contain either north, south, east or west as its values.
					Every route operates in 2 directions either noth/south or east/west.
					So this paramters helps us know which route exactly out of two routes
					a bus serve is the user checking for.

USAGE

	perl next_bus.pl -Bus_Route "metro blue line" -Bus_Stop_Name "target field station platform 1" -Direction "south"

COVERAGE

	The current script users do not have to remember the full Route name or the full bus stop name , even a sub string that
	is unique to them would give the desired results.

NOTES

	User must give all the 3 required parameters and also should make sure that if they are giving the sub string for 
	bus route and bus stop name , they MUST give a unique sub string that is contained only in those respectively.


EXAMPLE_OUTPUT

	Input => perl next_bus.pl -Bus_Route "metro blue line" -Direction "South" -Bus_Stop_Name "target field station platform 1"
	Output =>
	Bus in : [15.28] mins
	Bus Time : 10:12
	Route Number : [901]

EXTRA_INFORMATION

List of all valid bus route values :


	METRO Blue Line
	METRO Green Line
	METRO Red Line
	2 - Franklin Av - Riverside Av - U of M - 8th St SE
	3 - U of M - Como Av - Energy Park Dr - Maryland Av
	4 - New Brighton - Johnson St - Bryant Av - Southtown
	5 - Brklyn Center - Fremont - 26th Av - Chicago - MOA
	6 - U of M - Hennepin - Xerxes - France - Southdale
	7 - Plymouth - 27Av - Midtown - 46St LRT - 34Av S
	9 - Glenwood Av - Wayzata Blvd - Cedar Lk Rd -46St LRT
	10 - Central Av - University Av - Northtown
	11 - Columbia Heights - 2nd St NE - 4th Av S
	14 - Robbinsdale-West Broadway-Bloomington Av
	16 - U of M - University Av - Midway
	17 - Minnetonka Blvd - Uptown - Washington St NE
	18 - Nicollet Av - South Bloomington
	19 - Olson Memorial Hwy - Penn Av N - Brooklyn Center
	21 - Uptown - Lake St - Selby  Av
	22 - Brklyn Ctr - Lyndale Av N - Cedar - 28th Av S - VA
	23 - Uptown - 38th St - Highland Village
	30 - Broadway Crosstown - Westgate Station
	32 - Robbinsdale - Lowry Av - Rosedale
	46 - 50St - 46St - 46St LRT- Highland Village
	54 - Ltd Stop - W 7St - Airport - MOA
	62 - Rice St - Little Canada - Shoreview - Signal Hills
	63 - Grand Av - Raymond Sta - Sunray - McKnight Rd
	64 - Payne - Maryland - White Bear Av - Maplewood
	65 - Dale St - Co Rd B - Rosedale
	67 - W Minnehaha - Raymond Sta - Hiawatha
	68 - Jackson St - Robert St - 5th Av - Inver Hills
	70 - St Clair Av - W 7St - Burns Av - Sunray
	71 - Little Canada - Edgerton - Concord - Inver Hills
	74 - 46St - Randolph - W 7St - E 7St - Sunray
	80 - Maplewood - White Bear Av - Sunray
	83 - HarMar Target - Lexington Av
	84 - Rosedale - Snelling - 46th St LRT - Sibley Plaza
	87 - Rosedale - U of M St Paul - Cleveland Av
	440 - Apple Valley-Cedar Grove-VA Hospital
	442 - Burnsville Center-Apple Valley
	444 - Savage-Burnsville-Mall of America
	445 - Eagan-Cedar Grove
	465 - Burnsville-Minneapolis-U of M
	495 - Shakopee - MOA
	515 - Southdale - 66th St - Bloomington Av - VA - MOA
	538 - Southdale - York Av - Southtown - 86th St - MOA
	539 - Norm Coll - France Av - 98St - MOA
	540 - Edina - Richfield - 77th St - MOA
	612 - former 12X Excelsior Blvd-Hopkins-Opus
	645 - Ltd Stop - Mound - Wayzata - Ridgedale - Mpls
	721 - Ltd Stop - Brooklyn Center - New Hope - Mpls
	722 - Brooklyn Ctr - Humboldt Av N - Shingle Creek Pkwy
	723 - Starlite - North Henn Comm College - Brooklyn Ctr
	724 - Ltd Stop - Target Campus - Starlite - Brooklyn Ctr
	906 - Light Rail Shuttle - Lindbergh - Humphrey
	921 - A Line Roseville-St Paul-Minneapolis
	960 - State Fair - Ltd Stop - Minneapolis - State Fair

List of some valid bus stops for route "Metro Blue Line" (This is just extra information to test few scenarios):

Target Field Station Platform 2
Target Field Station Platform 1
Warehouse District\/ Hennepin Ave Station
Nicollet Mall Station
Government Plaza Station
U.S. Bank Stadium Station
Cedar-Riverside Station
Franklin Ave Station
38th St Station 
46th St Station 
VA Medical Center Station
Fort Snelling Station
MSP Airport Terminal 1 - Lindbergh Station
MSP Airport Terminal 2 - Humphrey Station
American Blvd Station 
Bloomington Central Station
28th Ave Station
Mall of America Station


	

