# This script is used to get the next bus timing for a given bus route,bus stop name and the
# direction such as north,soth,east,west
# usage - perl next_bus.pl -Bus_Route "metro blue line" -Bus_Stop_Name "target field station 
# platform 1" -Direction "south"

#----------------------------------------------------------------------

	# SYNOPSIS
		# This script is used to get the next bus timing for a given bus route,bus stop name and the
		# direction such as north,soth,east,west

	# PARAMETERS
		
		# Bus_Route 			- String
										# Bus route or sub string of the bus route

		# Bus_Stop_Name      - String
										# Bus stop name or sub string of the bus
										# stop name

	   # Direction 			- String 
	   								# Direction such as North, South, East, West


	# NOTES

		# When the substrings are specified make sure that it is unique to the
		# given String

		# For more documentation refer to README doc

	# USAGE 

		# perl next_bus.pl -Bus_Route "metro blue line" -Bus_Stop_Name "target field station 
		# platform 1" -Direction "south"

#----------------------------------------------------------------------

#! /usr/bin/perl

use strict;
use warnings;

use LWP::Simple;

my %args = @ARGV;

my $bus_route 	   = $args{-Bus_Route};
my $bus_stop_name = $args{-Bus_Stop_Name};
my $direction     = $args{-Direction};
my $bus_time;
my $route_num;
my $time_stamp;

&next_bus (
   Bus_Route 	  => $bus_route,
   Bus_Stop_Name => $bus_stop_name,
   Direction     => $direction,
   Next_Bus_In   => \$bus_time,
   Route_Num     => \$route_num,
   Time_Stamp    => \$time_stamp,
	);

 if($bus_time) {
 	printf("Bus in : [%.2f] mins\n",$bus_time);
 	print ("Bus Time : $time_stamp\n");
 	print ("Route Number : [$route_num]");
 }
 else {
 	print("");
 }

# subroutine for fetching the departure time of next bus
sub next_bus {

	START:

   my %params = @_;

   my $bus_route 		 = $params{Bus_Route};
   my $bus_stop_name  = $params{Bus_Stop_Name};
   my $direction      = $params{Direction};
   my $time_ref		 = $params{Next_Bus_In};
   my $route_num_ref  = $params{Route_Num};
   my $time_stamp_ref = $params{Time_Stamp};

   my $route_number   = 0;
   my $direction_num  = 0;
   my $stop_val 		 = "";
   my $departure_time = 0;
   my $epoch_time 	 = time; #gets epoch time
   my $time_stamp 	 = 0;

	my %route_info_hash;
	my %direction_hash;
	my %stops_info_hash;

   my $routes_url 	 = "http://svc.metrotransit.org/NexTrip/Routes?format=json";
   my $directions_url = "http://svc.metrotransit.org/NexTrip/Directions/$route_number?format=json";
   my $stops_url		 = "http://svc.metrotransit.org/NexTrip/Stops/$route_number/$direction_num?format=json";
   my $departure_url  = "http://svc.metrotransit.org/NexTrip/$route_number/$direction_num/$stop_val?format=json";


   my $routes_info = get("http://svc.metrotransit.org/NexTrip/Routes?format=json");
	chomp($routes_info);

	# Removing the space in search keywords which enables to search the string when only sub string
	# of bus_route is given
	$routes_info =~ s/\s//g;
	if(defined $bus_route) {
		$bus_route =~ s/\s//g;
	}
	else {
		print "ERROR - Please check the PARAMETERS, the script requires 3 PARAMETERS Bus_Route, Bus_Stop_Name and Direction";
		exit;
	}


	#	Grabs all the route numbers and its description from web and stores it in a hash
	while ($routes_info =~ /{"Description":"(.*?)","ProviderID":"\d+","Route":"(\d+)"}/g) {
		$route_info_hash{$2} = $1;
	}

	# Checks if the user mentioned route is a valid one or not, and if it is valid it grabs the 
	# route number of that mentioned route.
	foreach my $route_key (keys %route_info_hash) {
		if($route_info_hash{$route_key} =~ /$bus_route/ig) {
			$route_number = $route_key;
		}
	}
	if($route_number==0) {
		print "Mentioned route doesnot exist!!,Please check again";
	}
	else {
		my $direction_string = get("http://svc.metrotransit.org/NexTrip/Directions/$route_number?format=json");

		# Grabs the directions of specified route number and stores in direction_hash 
		while ($direction_string =~ /{"text":"(\w+)BOUND","Value":"(\d+)"/ig) {
			$direction_hash{$2} = $1;
		}

		# Checks if the mentioned direction is a valid one for the route mentioned.
		# Eg. Route 901 operates in North and South only and if user mentioned east as
		# its input then it is not valid input.
		foreach my $direction_key (keys %direction_hash) {
			if(defined $direction) {

				# Checks that input can only be either north/south/east/west
				if($direction =~ /north|south|east|west/i ) {
					if($direction_hash{$direction_key} =~ /$direction/i) {
						$direction_num = $direction_key;
					}
				}
				else {
					print "ERROR - Valid values for Direction is either North,South,East,West";
					exit;
				}
			}
			else {
				print "ERROR - Please check the PARAMETERS, the script requires 3 PARAMETERS Bus_Route, Bus_Stop_Name and Direction";
				exit;
			}
		}
		if($direction_num == 0) {
			print "the route [$bus_route] doesnot operate in [$direction]";
		}
		else {
			my $stops_info = get("http://svc.metrotransit.org/NexTrip/Stops/$route_number/$direction_num?format=json");
			chomp($stops_info);
			$stops_info =~ s/\s//g;
			if(defined $bus_stop_name) {
				$bus_stop_name =~ s/\s//g;
			}
			else {
				print "ERROR - Please check the PARAMETERS, the script requires 3 PARAMETERS Bus_Route, Bus_Stop_Name and Direction";
				exit;
			}
			

			# Grabs all the stops of a given route in a given direction and stores in a hash
			while ($stops_info =~ /{"text":"(.*?)","Value":"(.*?)"}/ig) {
				$stops_info_hash{$2} =$1;
			}

			# Grabs the unique value of the bus stop for given route and direction
			foreach my $route_hash_key (keys %stops_info_hash) {
				if ($stops_info_hash{$route_hash_key} =~ /$bus_stop_name/ig) {
					$stop_val = $route_hash_key;
				}
			}
			if (defined $stop_val) {
				my $departure_time_info = get("http://svc.metrotransit.org/NexTrip/$route_number/$direction_num/$stop_val?format=json");

				# Grabs the first line of the web which gives the next bus information such as departure time 
				if($departure_time_info =~ /{"Actual".*?,"DepartureText":"(.*?)","DepartureTime":.*?Date\((\d+)-0500\).*?",/ig) {
					$departure_time = $2/1000;
					$departure_time -= ($epoch_time+18);
					$departure_time /= 60;
					$time_stamp = $1;

					# there are situations when the bus just left and shows time left in negative seconds , so inorder
					# to avoid that the script sleeps for 3 seconds and reruns the whole thing again - this is in effort
					# not to confuse the users and reduce there work of re-running it again
					if($departure_time =~ /-/g || $departure_time == 0) {
						sleep(2);
						goto START;
					}
				}
				elsif($departure_time_info =~ /\[\]/g) {
					print ("There is no next bus available, all buses left for the day");
				}
			}
				$$time_ref 	     = $departure_time;
				$$route_num_ref  = $route_number;
				$$time_stamp_ref = $time_stamp;
		}
	}
}